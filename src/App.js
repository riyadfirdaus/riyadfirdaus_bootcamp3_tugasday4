import React, { useEffect, useState } from "react";
import "./App.css";
import UserItem from "./components/UserItem";
import {
  Modal,
  TextField,
  Box,
  Button,
  AppBar,
  Toolbar,
  Typography,
  Stack,
  Divider,
} from "@mui/material/";

function App() {
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
    borderRadius: 5,
  };

  const [user, setUser] = useState({});
  const [userList, setUserList] = useState([
    { name: "Riyad", address: "Cirebon", hobby: "Ngoding" },
    { name: "Andi", address: "Jakarta", hobby: "Membaca" },
    { name: "Budi", address: "Surabaya", hobby: "Tidur" },
  ]);
  const [filteredUser, setFilteredUser] = useState(userList);
  const [modalTrigger, setModalTrigger] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [currentIndex, setCurrentIndex] = useState();

  useEffect(() => {
    setFilteredUser(userList);
    console.log(userList);
  }, [userList]);

  function filterUser(value) {
    setFilteredUser(
      userList.filter((user) =>
        user.name.toLowerCase().includes(value.toLowerCase())
      )
    );
  }

  function addUser(user) {
    setUserList((userList) => [...userList, user]);
    setUser({});
  }

  function updateUser(newUser) {
    setUserList(
      userList.map((user) => {
        if (userList.indexOf(user) === currentIndex) {
          return newUser;
        } else return user;
      })
    );
  }

  function handleSave(user) {
    if (editMode) {
      updateUser(user);
      setEditMode(false);
    } else {
      addUser(user);
    }
  }

  function handleEdit(user) {
    setUser(user);
    setCurrentIndex(userList.indexOf(user));
    setEditMode(true);
    openModal();
  }

  function openModal() {
    setModalTrigger(true);
  }
  function closeModal() {
    setModalTrigger(false);
    setUser({});
  }

  return (
    <div className="App">
      <div className="container">
        {/* Header */}
        <AppBar position="static">
          <Toolbar sx={{ justifyContent: "space-between" }}>
            <Typography variant="h6" component="div" sx={{}}>
              Tugas Day 4
            </Typography>
            <Button onClick={openModal} color="inherit">
              Add User
            </Button>
          </Toolbar>
        </AppBar>

        {/* Search Bar */}
        <TextField
          variant="outlined"
          label="search"
          onChange={(e) => filterUser(e.target.value)}
          sx={{ m: 2 }}
        />

        {/* Buat tampilin user */}
        <Stack divider={<Divider orientation="horizontal" flexItem />}>
          {filteredUser.map((user, index) => {
            return (
              <UserItem
                key={index}
                name={user.name}
                address={user.address}
                hobby={user.hobby}
                function={() => {
                  handleEdit(user);
                }}
              />
            );
          })}
        </Stack>
      </div>

      {/* Modal */}
      <Modal
        open={modalTrigger}
        onClose={closeModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        {/* Modal Input */}
        <Box sx={style}>
          <Box>
            <TextField
              defaultValue={user.name}
              label="Name"
              fullWidth
              variant="outlined"
              onChange={(e) => setUser({ ...user, name: e.target.value })}
            />
            <TextField
              sx={{ mt: 1 }}
              defaultValue={user.address}
              label="Address"
              fullWidth
              variant="outlined"
              onChange={(e) => setUser({ ...user, address: e.target.value })}
            />
            <TextField
              sx={{ mt: 1 }}
              defaultValue={user.hobby}
              label="Hobby"
              fullWidth
              variant="outlined"
              onChange={(e) => setUser({ ...user, hobby: e.target.value })}
            />
          </Box>
          {/* Tombol Save dan Close */}
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <Button
              sx={{ mt: 1, mr: 1 }}
              onClick={() => {
                handleSave(user);
                closeModal();
              }}
              variant="contained"
              color="success"
            >
              Save
            </Button>
            <Button
              sx={{ mt: 1 }}
              onClick={closeModal}
              variant="contained"
              color="error"
            >
              Close
            </Button>
          </Box>
        </Box>
      </Modal>
    </div>
  );
}

export default App;
