import { Box, Button, Grid, Typography } from "@mui/material";
import React from "react";
import style from "./UserItem.module.css";

function UserItem(props) {
  return (
    <Box className={style.container}>
      <div className={style.name}>{props.name}</div>
      <div className={style.address}>{props.address}</div>
      <div className={style.hobby}>{props.hobby}</div>
      <Button
        className={style.btn}
        variant="contained"
        size="small"
        color="primary"
        onClick={props.function}
      >
        Edit
      </Button>
    </Box>
  );
}

export default UserItem;
