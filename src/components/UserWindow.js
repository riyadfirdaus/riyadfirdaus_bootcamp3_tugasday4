import { Box, Stack, Divider } from "@mui/material";
import React from "react";
import UserItem from "./UserItem";
import cssStyle from "./UserWindow.module.css";

function UserWindow(props) {
  const style = {
    display: "flex",
    flexDirection: "column",
  };

  console.log(props.userList);
  if (props.userList.length === 0) {
    return <div className={cssStyle.empty}>0 User</div>;
  } else
    return (
      <Stack divider={<Divider orientation="horizontal" flexItem />}>
        {props.userList.map((user, index) => {
          return (
            <UserItem
              key={index}
              name={user.name}
              address={user.address}
              hobby={user.hobby}
              function={props.function}
            />
          );
        })}
      </Stack>
    );
}

export default UserWindow;
